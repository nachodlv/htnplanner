// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HTNPlannerGameMode.generated.h"

UCLASS(minimalapi)
class AHTNPlannerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AHTNPlannerGameMode();
};



