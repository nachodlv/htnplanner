// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class HTNPlanner : ModuleRules
{
	public HTNPlanner(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[]
		{
			"Core", 
			"CoreUObject", 
			"Engine", 
			"InputCore", 
			"HeadMountedDisplay", 
			"AIModule",
			"GameplayAbilities",
			"GameplayTasks",
			"GameplayTags"
		});
		
		PrivateDependencyModuleNames.AddRange(new string[]
		{
			"NHTN"
		});
	}
}
