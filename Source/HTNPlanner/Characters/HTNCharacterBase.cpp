﻿#include "HTNCharacterBase.h"

// UE Includes
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "GameplayAbilities/Public/AbilitySystemComponent.h"

// HTN Includes
#include "HTNPlanner/AI/HTNAIGlobal.h"
#include "HTNPlanner/Pickups/HTNPickup.h"

namespace HTNCharacterUtils
{
	UBlackboardComponent* GetBlackboardFromCharacter(const APawn* Character)
	{
		return 	Character->GetController<AAIController>()->GetBlackboardComponent();
	}
}

AHTNCharacterBase::AHTNCharacterBase()
{
	AbilitySystemComponent = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("AbilitySystem"));
}

void AHTNCharacterBase::GrabPickup(AHTNPickup* Pickup)
{
	if (!ensureMsgf(!PickupGrabbed, TEXT("A pickup was already grabbed")))
	{
		return;
	}
	PickupGrabbed = Pickup;
	PickupGrabbed->PickupGrabbed(this);
	UBlackboardComponent* BBComp = HTNCharacterUtils::GetBlackboardFromCharacter(this);
	BBComp->SetValueAsObject(UHTNAIGlobal::GrabbedPickupBBKey, Pickup);
}

void AHTNCharacterBase::DropPickup()
{
	PickupGrabbed->PickupDropped();
	PickupGrabbed = nullptr;
	HTNCharacterUtils::GetBlackboardFromCharacter(this)->ClearValue(UHTNAIGlobal::GrabbedPickupBBKey);
}

void AHTNCharacterBase::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	if (InitialGameplayEffect)
	{
		const UGameplayEffect* InitialEffect = InitialGameplayEffect->GetDefaultObject<UGameplayEffect>();
		const FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
		AbilitySystemComponent->ApplyGameplayEffectToSelf(InitialEffect, 1.0f, EffectContext);
	}
}
