﻿#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameplayAbilities/Public/AbilitySystemInterface.h"

#include "HTNCharacterBase.generated.h"

class AHTNPickup;
class UGameplayEffect;
class UAbilitySystemComponent;

UCLASS()
class HTNPLANNER_API AHTNCharacterBase : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	AHTNCharacterBase();

	// ~ Begin IAbilitySystemInterface
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override { return AbilitySystemComponent; }
	// ~ End IAbilitySystemInterface

	/** Grabs the given pickup and stores it */
	void GrabPickup(AHTNPickup* Pickup);

	/** Stops storing the pickup */
	void DropPickup();

	AHTNPickup* GetPickup() const { return PickupGrabbed; }

	UFUNCTION(BlueprintCallable)
	void AddScore(float InScore) { Score += InScore; }

	UFUNCTION(BlueprintPure)
	float GetScore() const { return Score; }

protected:
	/** Apply initial abilties */
	virtual void PostInitializeComponents() override;
	
private:
	UPROPERTY(VisibleAnywhere, Category = "HTN")
	TObjectPtr<UAbilitySystemComponent> AbilitySystemComponent;

	UPROPERTY(EditAnywhere, Category = "HTN")
	TSubclassOf<UGameplayEffect> InitialGameplayEffect;

	/** The current pickup being stored */
	UPROPERTY(Transient)
	TObjectPtr<AHTNPickup> PickupGrabbed = nullptr;

	float Score = 0.0f;
};
