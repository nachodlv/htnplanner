﻿#pragma once

// UE Includes
#include "CoreMinimal.h"

// NHTN Includes
#include "Actors/NHTNAIController.h"

#include "HTNAIController.generated.h"

UCLASS()
class HTNPLANNER_API AHTNAIController : public ANHTNAIController
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	virtual void OnPossess(APawn* InPawn) override;
};
