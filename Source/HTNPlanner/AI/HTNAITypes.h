﻿#pragma once

#include "CoreMinimal.h"

#define HTN_ADD_SELECTOR_LOCATION_FILTER(Class, KeySelector)\
	KeySelector.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(Class, KeySelector), AActor::StaticClass());\
	KeySelector.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(Class, KeySelector))


class UBlackboardComponent;
struct FBlackboardKeySelector;

/** Static helper for AI */
class FHTNAIHelper
{
public:
	static FString DescribeLocation(const UBlackboardComponent& BBComp, const FName& LocationKeyName);
};
