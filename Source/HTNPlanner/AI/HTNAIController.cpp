﻿#include "HTNAIController.h"

// UE Includes
#include "BehaviorTree/BlackboardComponent.h"

// HTN Includes
#include "HTNAIGlobal.h"


void AHTNAIController::BeginPlay()
{
	Super::BeginPlay();

	if (const APlayerController* Player = GetWorld()->GetFirstPlayerController())
	{
		UBlackboardComponent* BBComp = GetBlackboardComponent();
		BBComp->SetValueAsObject(UHTNAIGlobal::PlayerBBKey, Player->GetPawn());
	}
}

void AHTNAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	RunHTN();
	UBlackboardComponent* BBComp = GetBlackboardComponent();
	BBComp->SetValueAsVector(UHTNAIGlobal::SelfLocationBBKey, GetPawn()->GetActorLocation());
}

