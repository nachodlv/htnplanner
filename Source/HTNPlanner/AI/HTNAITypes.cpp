﻿#include "HTNAITypes.h"

// UE Includes
#include "BehaviorTree/BlackboardComponent.h"

FString FHTNAIHelper::DescribeLocation(const UBlackboardComponent& BBComp, const FName& LocationKeyName)
{
	FString PositionDebug;
	if (const AActor* GoalActor = Cast<AActor>(BBComp.GetValueAsObject(LocationKeyName)))
	{
		PositionDebug = FString::Printf(TEXT("%s: "), *GoalActor->GetName());
	}
	FVector Location;
	if (BBComp.GetLocationFromEntry(LocationKeyName, Location))
	{
		PositionDebug += Location.ToString();
	}
	return PositionDebug;
}
