﻿#include "HTNEQT_Random.h"

UHTNEQT_Random::UHTNEQT_Random()
{
	ValidItemType = UEnvQueryItemType::StaticClass();
	FloatValueMax.DefaultValue = 1.0f;
	FloatValueMin.DefaultValue = 0.0f;
	SetWorkOnFloatValues(true);
}

void UHTNEQT_Random::RunTest(FEnvQueryInstance& QueryInstance) const
{
	const UObject* QueryOwner = QueryInstance.Owner.Get();

	FloatValueMax.BindData(QueryOwner, QueryInstance.QueryID);
	FloatValueMin.BindData(QueryOwner, QueryInstance.QueryID);
	const float MaxValue = FloatValueMax.GetValue();
	const float MinValue = FloatValueMin.GetValue();
	
	for (FEnvQueryInstance::ItemIterator It(this, QueryInstance); It; ++It)
	{
		const float RandScore = FMath::RandRange(0.0f, 1.0f);
		It.SetScore(TestPurpose, FilterType, RandScore, MinValue, MaxValue);
	}
}
