﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "HTNEQT_CanBePickup.h"

// UE Includes
#include "EnvironmentQuery/Contexts/EnvQueryContext_Querier.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"

// HTN Includes
#include "HTNPlanner/Characters/HTNCharacterBase.h"
#include "HTNPlanner/Pickups/HTNPickup.h"

UHTNEQT_CanBePickup::UHTNEQT_CanBePickup()
{
	ValidItemType = UEnvQueryItemType_Actor::StaticClass();
	SetWorkOnFloatValues(false);
	Picker = UEnvQueryContext_Querier::StaticClass();
}

void UHTNEQT_CanBePickup::RunTest(FEnvQueryInstance& QueryInstance) const
{
	check(QueryInstance.Owner.IsValid());
	
	BoolValue.BindData(QueryInstance.Owner.Get(), QueryInstance.QueryID);
	const bool bExpectedValue = BoolValue.GetValue();

	TArray<AActor*> PossiblePickers;
	if (!QueryInstance.PrepareContext(Picker, PossiblePickers))
	{
		return;
	}
	
	for (FEnvQueryInstance::ItemIterator It(this, QueryInstance); It; ++It)
	{
		AHTNPickup* Pickup = Cast<AHTNPickup>(QueryInstance.GetItemAsActor(It.GetIndex()));
		if (!ensureMsgf(Pickup, TEXT("UHTNEQT_CanBePickup can only be used with pickup items")))
		{
			It.ForceItemState(EEnvItemStatus::Failed);
			continue;
		}

		for (AActor* PossiblePicker : PossiblePickers)
		{
			AHTNCharacterBase* CharacterPicker = Cast<AHTNCharacterBase>(PossiblePicker);
			if (ensure(CharacterPicker))
			{
				It.SetScore(TestPurpose, FilterType, Pickup->CanBePickedUp(CharacterPicker), bExpectedValue);
			}
		}
	}
}
