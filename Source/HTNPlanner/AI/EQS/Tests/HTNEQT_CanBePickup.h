﻿#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryTest.h"
#include "UObject/Object.h"

#include "HTNEQT_CanBePickup.generated.h"

/** Checks whether a pick up can be picked up by the "Picker" */
UCLASS()
class HTNPLANNER_API UHTNEQT_CanBePickup : public UEnvQueryTest
{
	GENERATED_BODY()

public:
	UHTNEQT_CanBePickup();

	/** Check whether the items from the query can be picked by the Picker */
	virtual void RunTest(FEnvQueryInstance& QueryInstance) const override;

private:
	/** Whom is going to do the pick up */
	UPROPERTY(EditAnywhere, Category = "HTN")
	TSubclassOf<UEnvQueryContext> Picker;

};
