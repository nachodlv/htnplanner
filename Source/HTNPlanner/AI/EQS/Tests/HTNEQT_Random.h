﻿#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryTest.h"

#include "HTNEQT_Random.generated.h"

/** Sets the item scores randomly */
UCLASS()
class HTNPLANNER_API UHTNEQT_Random : public UEnvQueryTest
{
	GENERATED_BODY()

public:
	UHTNEQT_Random();
	
	virtual void RunTest(FEnvQueryInstance& QueryInstance) const override;
};
