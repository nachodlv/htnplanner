﻿#include "HTNIsAtLocationDecorator.h"

// HTN Includes
#include "HTNPlanner/AI/HTNAITypes.h"

UHTNIsAtLocationDecorator::UHTNIsAtLocationDecorator()
{
	HTN_ADD_SELECTOR_LOCATION_FILTER(UHTNIsAtLocationDecorator, CurrentLocation);
	HTN_ADD_SELECTOR_LOCATION_FILTER(UHTNIsAtLocationDecorator, IntendedLocation);
}

bool UHTNIsAtLocationDecorator::CalculateRawCondition(const UNHTNBlackboardComponent& WorldState) const
{
	FVector Location;
	if (!WorldState.GetLocationFromEntry(CurrentLocation.SelectedKeyName, Location))
	{
		return false;
	}
	FVector Goal;
	if (!WorldState.GetLocationFromEntry(IntendedLocation.SelectedKeyName, Goal))
	{
		return false;
	}

	return Location.Equals(Goal, AcceptanceRadius);
}

FString UHTNIsAtLocationDecorator::GetTitleDescription() const
{
	return FString::Printf(TEXT("%s : %.2f units from %s to %s"), *Super::GetTitleDescription(), AcceptanceRadius,
		*CurrentLocation.SelectedKeyName.ToString(), *IntendedLocation.SelectedKeyName.ToString());
}

#if WITH_GAMEPLAY_DEBUGGER
FString UHTNIsAtLocationDecorator::GetRuntimeDescription(const UNHTNBlackboardComponent& WorldState) const
{
	FString DebugDistance = "?";
	FVector Location;
	FVector Goal;
	if (WorldState.GetLocationFromEntry(CurrentLocation.SelectedKeyName, Location))
	{
		if (WorldState.GetLocationFromEntry(IntendedLocation.SelectedKeyName, Goal))
		{
			DebugDistance = FString::SanitizeFloat(FVector::Distance(Location, Goal));
		}
	}
	
	return FString::Printf(TEXT("Distance: %s"), *DebugDistance);
}
#endif // WITH_GAMEPLAY_DEBUGGER

