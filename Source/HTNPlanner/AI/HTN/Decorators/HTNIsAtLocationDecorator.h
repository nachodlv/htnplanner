﻿#pragma once

#include "CoreMinimal.h"

#include "Tasks/NHTNDecorator.h"

#include "UObject/Object.h"
#include "HTNIsAtLocationDecorator.generated.h"

/** Checks whether the CurrentLocation is in a certain distance from the IntendedLocation */
UCLASS()
class HTNPLANNER_API UHTNIsAtLocationDecorator : public UNHTNDecorator
{
	GENERATED_BODY()

	UHTNIsAtLocationDecorator();

protected:
	/** Checks if the CurrentLocation and the IntendedLocation are equals */
	virtual bool CalculateRawCondition(const UNHTNBlackboardComponent& WorldState) const override;

	virtual FString GetTitleDescription() const override;

#if WITH_GAMEPLAY_DEBUGGER
	virtual FString GetRuntimeDescription(const UNHTNBlackboardComponent& WorldState) const override;
#endif WITH_GAMEPLAY_DEBUGGER
	
private:
	/** Goal location */
	UPROPERTY(EditAnywhere, Category = "HTN")
	FBlackboardKeySelector IntendedLocation;

	/** The location to check with */
	UPROPERTY(EditAnywhere, Category = "HTN")
	FBlackboardKeySelector CurrentLocation;

	/** How far the CurrentLocation from the IntendedLocation can be */
	UPROPERTY(EditAnywhere, Category = "HTN")
	float AcceptanceRadius = 10.0f;
};
