﻿#include "HTNMoveToTask.h"

// UE Includes
#include "AIController.h"

// NHTN Includes
#include "Components/NHTNComponent.h"

// HTN Includes
#include "HTNPlanner/AI/HTNAITypes.h"
#include "HTNPlanner/AI/HTNAIGlobal.h"

UHTNMoveToTask::UHTNMoveToTask()
{
	AcceptanceRadius = UPathFollowingComponent::DefaultAcceptanceRadius;

	HTN_ADD_SELECTOR_LOCATION_FILTER(UHTNMoveToTask, MoveToPosition);
}

bool UHTNMoveToTask::CanBeExecuted(const UNHTNBlackboardComponent& WorldState) const
{
	return WorldState.TryGetLocationFromEntry(WorldState.GetKeyID(MoveToPosition.SelectedKeyName)).IsSet();
}

ENHTNTaskStatus UHTNMoveToTask::ExecuteTask(UNHTNComponent& HTNComp)
{
	const UBlackboardComponent* BBComp = HTNComp.GetBlackboardComponent();
	FVector Location;
	BBComp->GetLocationFromEntry(MoveToPosition.SelectedKeyName, Location);
	
	AAIController* AIController = HTNComp.GetAIOwner();
	FAIMoveRequest MoveRequest;
	if (const AActor* GoalActor = Cast<AActor>(BBComp->GetValueAsObject(MoveToPosition.SelectedKeyName)))
	{
		MoveRequest.SetGoalActor(GoalActor);
	}
	else
	{
		MoveRequest.SetGoalLocation(BBComp->GetValueAsVector(MoveToPosition.SelectedKeyName));
	}
	MoveRequest.SetUsePathfinding(bUsePathfinding);
	MoveRequest.SetAllowPartialPath(bAllowPartialPath);
	MoveRequest.SetProjectGoalLocation(bProjectGoalOnNavigation);
	MoveRequest.SetReachTestIncludesGoalRadius(bReachTestIncludesGoalRadius);
	MoveRequest.SetReachTestIncludesAgentRadius(bReachTestIncludesAgentRadius);
	MoveRequest.SetCanStrafe(bCanStrafe);
	MoveRequest.SetAcceptanceRadius(AcceptanceRadius);

	FNavPathSharedPtr Path;
	const FPathFollowingRequestResult Result = AIController->MoveTo(MoveRequest);
	if (Result.Code == EPathFollowingRequestResult::Failed)
	{
		return ENHTNTaskStatus::Failed;
	}
	if (Result.Code == EPathFollowingRequestResult::AlreadyAtGoal)
	{
		return ENHTNTaskStatus::Success;
	}

	RequestID = Result.MoveId;
	CachedHTNComp = &HTNComp;
	
	RegisterMessageObserver(HTNComp, UBrainComponent::AIMessage_MoveFinished, Result.MoveId);
	RegisterMessageObserver(HTNComp, UBrainComponent::AIMessage_RepathFailed);

	return ENHTNTaskStatus::InProgress;
}

void UHTNMoveToTask::AbortTask(UNHTNComponent& HTNComp)
{
	AAIController* Controller = HTNComp.GetAIOwner();
	if (Controller->GetCurrentMoveRequestID() == RequestID)
	{
		Controller->StopMovement();
	}
	Super::AbortTask(HTNComp);
}

void UHTNMoveToTask::ApplyEffects(UBlackboardComponent& WorldState) const
{
	const APawn* Pawn = WorldState.GetOwner<AAIController>()->GetPawn();
	WorldState.SetValueAsVector(UHTNAIGlobal::SelfLocationBBKey,  Pawn->GetActorLocation());
}

void UHTNMoveToTask::ApplyExpectedEffects(UNHTNBlackboardComponent& WorldState) const
{
	FVector GoalLocation;
	WorldState.GetLocationFromEntry(MoveToPosition.SelectedKeyName, GoalLocation);
	WorldState.SetValueAsVector(UHTNAIGlobal::SelfLocationBBKey,  GoalLocation);
}

void UHTNMoveToTask::OnMessage(UNHTNComponent& HTNComp, const FAIMessage& Message)
{
	const bool bSuccess = Message.Status != FAIMessage::Failure;
	FinishLatentTask(HTNComp, bSuccess ? ENHTNTaskStatus::Success : ENHTNTaskStatus::Failed);
}

FString UHTNMoveToTask::GetTitleDescription() const
{
	return FString::Printf(TEXT("%s: Move to %s"), *Super::GetTitleDescription(), *MoveToPosition.SelectedKeyName.ToString());
}

#if WITH_GAMEPLAY_DEBUGGER
FString UHTNMoveToTask::GetRuntimeDescription(const UNHTNBlackboardComponent& WorldState) const
{
	return FHTNAIHelper::DescribeLocation(WorldState, MoveToPosition.SelectedKeyName);
}

#endif // WITH_GAMEPLAY_DEBUGGER

