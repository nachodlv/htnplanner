﻿#pragma once

// UE Includes
#include "CoreMinimal.h"

// NHTN Includes
#include "Tasks/NHTNPrimitiveTask.h"

#include "HTNPrintStringTask.generated.h"

UCLASS()
class HTNPLANNER_API UHTNPrintStringTask : public UNHTNPrimitiveTask
{
	GENERATED_BODY()

public:
	virtual ENHTNTaskStatus ExecuteTask(UNHTNComponent& HTNComp) override;

	virtual FString GetTitleDescription() const override;

private:
	UPROPERTY(EditAnywhere)
	FString TextToPrint = TEXT("Hello World!");
};
