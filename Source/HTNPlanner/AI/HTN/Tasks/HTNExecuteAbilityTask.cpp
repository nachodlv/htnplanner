﻿#include "HTNExecuteAbilityTask.h"

// UE Includes
#include "AIController.h"
#include "AbilitySystemComponent.h"
#include "AbilitySystemInterface.h"

// NHTN Includes
#include "Components/NHTNComponent.h"

namespace HTNExecuteAbilityTaskUtils
{
	UAbilitySystemComponent* GetASCFromHTNComp(const UNHTNComponent& HTNComp)
	{
		const IAbilitySystemInterface* AbilitySystemInterface = HTNComp.GetAIOwner()->GetPawn<IAbilitySystemInterface>();
		return AbilitySystemInterface->GetAbilitySystemComponent();
	}
}

ENHTNTaskStatus UHTNExecuteAbilityTask::ExecuteTask(UNHTNComponent& HTNComp)
{
	Super::ExecuteTask(HTNComp);
	UAbilitySystemComponent* ASC = HTNExecuteAbilityTaskUtils::GetASCFromHTNComp(HTNComp);
	if (!ASC->TryActivateAbilityByClass(AbilityToExecute.LoadSynchronous()))
	{
		return ENHTNTaskStatus::Failed;
	}
	if (bFireAndForget)
	{
		return ENHTNTaskStatus::Success;
	}
	AbilityEndedHandle = ASC->OnAbilityEnded.AddUObject(this, &UHTNExecuteAbilityTask::TryFinishLatentTask, &HTNComp);
	return ENHTNTaskStatus::InProgress;
}

void UHTNExecuteAbilityTask::AbortTask(UNHTNComponent& HTNComp)
{
	UAbilitySystemComponent* ASC = HTNExecuteAbilityTaskUtils::GetASCFromHTNComp(HTNComp);
	ASC->OnAbilityEnded.Remove(AbilityEndedHandle);
}

void UHTNExecuteAbilityTask::TryFinishLatentTask(const FAbilityEndedData& AbilityEndedData, UNHTNComponent* HTNComp)
{
	if (AbilityEndedData.AbilityThatEnded->IsA(AbilityToExecute.LoadSynchronous()))
	{
		FinishLatentTask(*HTNComp, AbilityEndedData.bWasCancelled ? ENHTNTaskStatus::Failed : ENHTNTaskStatus::Success);
	}
}
