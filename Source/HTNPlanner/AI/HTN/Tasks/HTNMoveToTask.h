﻿#pragma once

// UE Includes
#include "AITypes.h"
#include "CoreMinimal.h"
#include "Navigation/PathFollowingComponent.h"

// NHTN Includes
#include "Tasks/NHTNPrimitiveTask.h"

#include "HTNMoveToTask.generated.h"

/** Moves the AI to the given MoveToPosition */
UCLASS()
class HTNPLANNER_API UHTNMoveToTask : public UNHTNPrimitiveTask
{
	GENERATED_BODY()

public:
	UHTNMoveToTask();

	/** Executes the move request to the MoveToPosition */
	virtual ENHTNTaskStatus ExecuteTask(UNHTNComponent& HTNComp) override;

	/** Checks whether the MoveToPosition is set in the blackboard */
	virtual bool CanBeExecuted(const UNHTNBlackboardComponent& WorldState) const override;

	/** Cancels the move request */
	virtual void AbortTask(UNHTNComponent& HTNComp) override;

	/** Sets the current AI position blackboard key to its actual position */
	virtual void ApplyEffects(UBlackboardComponent& WorldState) const override;

	/** Sets the current AI position blackboard key to the MoveToPosition location */
	virtual void ApplyExpectedEffects(UNHTNBlackboardComponent& WorldState) const override;

	virtual void OnMessage(UNHTNComponent& HTNComp, const FAIMessage& Message) override;

	virtual FString GetTitleDescription() const override;

#if WITH_GAMEPLAY_DEBUGGER
	virtual FString GetRuntimeDescription(const UNHTNBlackboardComponent& WorldState) const override;
#endif // WITH_GAMEPLAY_DEBUGGER

private:
	/** The location the AI will move to */
	UPROPERTY(EditAnywhere, Category = "HTN")
	FBlackboardKeySelector MoveToPosition;

	UPROPERTY(EditAnywhere, Category = "HTN|Config")
	bool bUsePathfinding = true;

	UPROPERTY(EditAnywhere, Category = "HTN|Config")
	bool bAllowPartialPath = false;

	UPROPERTY(EditAnywhere, Category = "HTN|Config")
	bool bProjectGoalOnNavigation = false;

	UPROPERTY(EditAnywhere, Category = "HTN|Config")
	bool bReachTestIncludesAgentRadius = true;

	UPROPERTY(EditAnywhere, Category = "HTN|Config")
	bool bReachTestIncludesGoalRadius = true;

	UPROPERTY(EditAnywhere, Category = "HTN|Config")
	bool bCanStrafe = false;

	UPROPERTY(EditAnywhere, Category = "HTN|Config")
	float AcceptanceRadius;

	/** The request we are currently executing */
	FAIRequestID RequestID;

	UPROPERTY(Transient)
	UNHTNComponent* CachedHTNComp = nullptr;
};
