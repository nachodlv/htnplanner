﻿#pragma once

// UE Includes
#include "Abilities/GameplayAbilityTypes.h"
#include "CoreMinimal.h"

// NHTN Includes
#include "Tasks/NHTNPrimitiveTask.h"

#include "HTNExecuteAbilityTask.generated.h"

class UGameplayAbility;

/** Executes a gameplay ability */
UCLASS()
class HTNPLANNER_API UHTNExecuteAbilityTask : public UNHTNPrimitiveTask
{
	GENERATED_BODY()

public:
	/** Executes the ability and waits for the end ability if necessary */
	virtual ENHTNTaskStatus ExecuteTask(UNHTNComponent& HTNComp) override;

	/** Removes the end ability delegate */
	virtual void AbortTask(UNHTNComponent& HTNComp) override;

protected:
	/** Finishes the task */
	void TryFinishLatentTask(const FAbilityEndedData& AbilityEndedData, UNHTNComponent* HTNComp);

private:
	/** The ability that will be executed */
	UPROPERTY(EditAnywhere, Category = "HTN")
	TSoftClassPtr<UGameplayAbility> AbilityToExecute;

	/** Whether the task should wait for the ability to finish */
	UPROPERTY(EditAnywhere, Category = "HTN")
	bool bFireAndForget = false;

	FDelegateHandle AbilityEndedHandle;
};
