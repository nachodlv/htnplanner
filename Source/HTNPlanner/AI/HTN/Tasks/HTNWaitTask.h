﻿#pragma once

// UE Includes
#include "CoreMinimal.h"

// NHTN Includes
#include "Tasks/NHTNPrimitiveTask.h"

#include "HTNWaitTask.generated.h"

/** Waits the given time before continue execution */
UCLASS()
class HTNPLANNER_API UHTNWaitTask : public UNHTNPrimitiveTask
{
	GENERATED_BODY()

public:
	virtual ENHTNTaskStatus ExecuteTask(UNHTNComponent& HTNComp) override;

	virtual void AbortTask(UNHTNComponent& HTNComp) override;

	virtual FString GetTitleDescription() const override;

#if WITH_GAMEPLAY_DEBUGGER
	virtual FString GetRuntimeDescription(const UNHTNBlackboardComponent& WorldState) const override;
#endif // WITH_GAMEPLAY_DEBUGGER

protected:
	void WaitTimeFinished(TWeakObjectPtr<UNHTNComponent> HTNComp);
	
private:
	/** The time the plan will need to wait to continue executing */
	UPROPERTY(EditAnywhere, Category = "HTN")
	float TimeToWait = 1.0f;

	FTimerHandle TimerHandle;
};
