﻿#pragma once

// UE Includes
#include "CoreMinimal.h"

// NHTN Includes
#include "Tasks/NHTNPrimitiveTask.h"

#include "HTNSetKeyTask.generated.h"

/** Sets a key from the blackboard using another one */
UCLASS()
class HTNPLANNER_API UHTNSetKeyTask : public UNHTNPrimitiveTask
{
	GENERATED_BODY()

public:
	/** Stores the value "From" in the key "To" */
	virtual void ApplyEffects(UBlackboardComponent& WorldState) const override;
	
	virtual void ApplyExpectedEffects(UNHTNBlackboardComponent& WorldState) const override;
	
private:
	/** From where the value will be retrieved */
	UPROPERTY(EditAnywhere, Category = "HTN")
	FBlackboardKeySelector From;

	/** Where the value will be stored */
	UPROPERTY(EditAnywhere, Category = "HTN")
	FBlackboardKeySelector To;
};
