﻿#include "HTNWaitTask.h"

//  NHTN Includes
#include "Components/NHTNComponent.h"

ENHTNTaskStatus UHTNWaitTask::ExecuteTask(UNHTNComponent& HTNComp)
{
	FTimerManager& TimerManager = HTNComp.GetWorld()->GetTimerManager();
	const TWeakObjectPtr<UNHTNComponent> WeakHTN = TWeakObjectPtr<UNHTNComponent>(&HTNComp);
	const FTimerDelegate Delegate = FTimerDelegate::CreateUObject(this, &UHTNWaitTask::WaitTimeFinished, WeakHTN);
	TimerManager.SetTimer(TimerHandle, Delegate, TimeToWait, false);
	return TimeToWait > 0.0f ? ENHTNTaskStatus::InProgress : ENHTNTaskStatus::Failed;
}

void UHTNWaitTask::AbortTask(UNHTNComponent& HTNComp)
{
	FTimerManager& TimerManager = HTNComp.GetWorld()->GetTimerManager();
	TimerManager.ClearTimer(TimerHandle);
}

FString UHTNWaitTask::GetTitleDescription() const
{
	return FString::Printf(TEXT("%s: Wait %.1f seconds"), *Super::GetTitleDescription(), TimeToWait);
}

#if WITH_GAMEPLAY_DEBUGGER
FString UHTNWaitTask::GetRuntimeDescription(const UNHTNBlackboardComponent& WorldState) const
{
	const FTimerManager& TimerManager = WorldState.GetWorld()->GetTimerManager();
	const float RemainingTime = TimerManager.GetTimerRemaining(TimerHandle);
	return FString::Printf(TEXT("Remaining: %.1f"), RemainingTime);
}

#endif // WITH_GAMEPLAY_DEBUGGER

void UHTNWaitTask::WaitTimeFinished(TWeakObjectPtr<UNHTNComponent> HTNComp)
{
	if (HTNComp.IsValid())
	{
		FinishLatentTask(*HTNComp, ENHTNTaskStatus::Success);
	}
}
