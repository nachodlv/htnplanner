﻿#include "HTNClearTask.h"

void UHTNClearTask::ApplyEffects(UBlackboardComponent& WorldState) const
{
	WorldState.ClearValue(KeyToClear.SelectedKeyName);
}

void UHTNClearTask::ApplyExpectedEffects(UNHTNBlackboardComponent& WorldState) const
{
	ApplyEffects(WorldState);
	WorldState.RemoveExpectedKeyToBeSet(KeyToClear.SelectedKeyName);
}
