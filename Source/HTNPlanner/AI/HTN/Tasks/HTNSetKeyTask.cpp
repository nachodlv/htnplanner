﻿#include "HTNSetKeyTask.h"

#include "BehaviorTree/Blackboard/BlackboardKeyType_Class.h"

#if WITH_EDITOR
void UHTNSetKeyTask::ApplyEffects(UBlackboardComponent& WorldState) const
{
	const FBlackboard::FKey FromKeyId = WorldState.GetKeyID(From.SelectedKeyName);
	const FBlackboard::FKey ToKeyId = WorldState.GetKeyID(To.SelectedKeyName);
	ensureMsgf(WorldState.CopyKeyValue(FromKeyId, ToKeyId), TEXT("Failed copying values"));
}

void UHTNSetKeyTask::ApplyExpectedEffects(UNHTNBlackboardComponent& WorldState) const
{
	ApplyEffects(WorldState);

	if (WorldState.IsKeyExpectedToBeSet(From.SelectedKeyName))
	{
		WorldState.AddExpectedKeyToBeSet(To.SelectedKeyName);
	}
	else
	{
		WorldState.RemoveExpectedKeyToBeSet(To.SelectedKeyName);
	}
}

#endif // WITH_EDITOR
