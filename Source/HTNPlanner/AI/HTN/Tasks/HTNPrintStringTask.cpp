﻿#include "HTNPrintStringTask.h"

#define HTN_PRINT_TASK_ID 123

ENHTNTaskStatus UHTNPrintStringTask::ExecuteTask(UNHTNComponent& HTNComp)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(HTN_PRINT_TASK_ID, 1.f, FColor::Green, *TextToPrint);
	}
	UE_LOG(LogTemp, Log, TEXT("%s"), *TextToPrint);
	return ENHTNTaskStatus::Success;
}

FString UHTNPrintStringTask::GetTitleDescription() const
{
	return FString::Printf(TEXT("%s, Print %s"), *Super::GetTitleDescription(), *TextToPrint);
}

#undef HTN_PRINT_TASK_ID
