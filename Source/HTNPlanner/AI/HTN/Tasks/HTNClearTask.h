﻿#pragma once

// UE Includes
#include "CoreMinimal.h"

// NHTN Includes
#include "Tasks/NHTNPrimitiveTask.h"

#include "HTNClearTask.generated.h"

/** Clears the value from a blackboard key */
UCLASS()
class HTNPLANNER_API UHTNClearTask : public UNHTNPrimitiveTask
{
	GENERATED_BODY()

public:
	/** Clears the key value */
	virtual void ApplyEffects(UBlackboardComponent& WorldState) const override;

	/** Clears the key value and removes it from the expected keys to be set */
	virtual void ApplyExpectedEffects(UNHTNBlackboardComponent& WorldState) const override;
	
private:
	/** The value from this key will be cleared */
	UPROPERTY(EditAnywhere, Category = "HTN")
	FBlackboardKeySelector KeyToClear;
};
