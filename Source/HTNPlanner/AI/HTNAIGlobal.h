﻿#pragma once

#include "CoreMinimal.h"
#include "HTNAIGlobal.generated.h"

UCLASS()
class HTNPLANNER_API UHTNAIGlobal : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	/** Used to store the player target */
	static FName PlayerBBKey;
	
	/** Used to store the AI current location */
	static FName SelfLocationBBKey;

	/** Used to store the pickup that will be grabbed */
	static FName PickupToGrabBBKey;

	/** Used to store the current pickup that is grabbed */
	static FName GrabbedPickupBBKey;
};
