﻿#include "HTNAIGlobal.h"

FName UHTNAIGlobal::PlayerBBKey = TEXT("Player");

FName UHTNAIGlobal::SelfLocationBBKey = TEXT("SelfLocation");

FName UHTNAIGlobal::PickupToGrabBBKey = TEXT("PickupToGrab");

FName UHTNAIGlobal::GrabbedPickupBBKey = TEXT("GrabbedPickup");
