﻿#include "HTNTeleportComponent.h"

// UE Includes
#include "EnvironmentQuery/EnvQueryManager.h"


UHTNTeleportComponent::UHTNTeleportComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = true;
}

void UHTNTeleportComponent::BeginPlay()
{
	Super::BeginPlay();
	PrimaryComponentTick.TickInterval = TeleportFrequency;
}

void UHTNTeleportComponent::TickComponent(float DeltaTime, ELevelTick TickType,
	FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	FQueryFinishedSignature Delegate = FQueryFinishedSignature::CreateUObject(this, &UHTNTeleportComponent::OnTeleportQueryFinished);
	TeleportQueryID = TeleportQuery.Execute(*GetOwner(), nullptr, Delegate);
	SetComponentTickEnabled(false);
}

void UHTNTeleportComponent::OnTeleportQueryFinished(TSharedPtr<FEnvQueryResult, ESPMode::ThreadSafe> EnvQueryResult)
{
	if (EnvQueryResult->IsSuccessful())
	{
		const FVector BestLocation = EnvQueryResult->GetItemAsLocation(0);
		AActor* Actor = GetOwner();
		Actor->TeleportTo(BestLocation, Actor->GetActorRotation());
	}
	SetComponentTickEnabled(true);
}

void UHTNTeleportComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (UEnvQueryManager* QueryManager = UEnvQueryManager::GetCurrent(GetWorld()))
	{
		QueryManager->AbortQuery(TeleportQueryID);
	}
	Super::EndPlay(EndPlayReason);
}



