﻿#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

// NHTN Includes
#include "Tasks/Common/PrimitiveTasks/NHTNExecuteEQSTask.h"

#include "HTNTeleportComponent.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class HTNPLANNER_API UHTNTeleportComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UHTNTeleportComponent();

protected:
	virtual void BeginPlay() override;
	
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	/** Executes the TeleportQuery */
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/** Teleports the actor owner with the best item */
	void OnTeleportQueryFinished(TSharedPtr<FEnvQueryResult, ESPMode::ThreadSafe> EnvQueryResult);

private:
	/** Used to retrieve the location to teleport to */
	UPROPERTY(EditAnywhere, Category = "HTN")
	FEQSParametrizedQueryExecutionRequest TeleportQuery;

	/** Time between teleports */
	UPROPERTY(EditAnywhere, Category = "HTN")
	float TeleportFrequency = 5.0f;

	int32 TeleportQueryID = INDEX_NONE;
};
