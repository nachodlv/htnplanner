﻿#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "HTNPickup.generated.h"

class AHTNCharacterBase;

UCLASS()
class HTNPLANNER_API AHTNPickup : public AActor
{
	GENERATED_BODY()


public:
	/** Returns whether the character can pick this pick up */
	UFUNCTION(BlueprintPure)
	bool CanBePickedUp(AHTNCharacterBase* Picker) const;
	
	UFUNCTION(BlueprintNativeEvent)
	void PickupGrabbed(AHTNCharacterBase* GrabbedBy);

	UFUNCTION(BlueprintNativeEvent)
	void PickupDropped();

	/** Returns who was the character that last pick this up */
	UFUNCTION(BlueprintPure)
	AHTNCharacterBase* GetLastGrabbedByActor() const { return LastGrabbedBy; }

private:
	/** The character that last pick this up */
	UPROPERTY(Transient)
	TObjectPtr<AHTNCharacterBase> LastGrabbedBy = nullptr;
};
