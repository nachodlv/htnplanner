﻿#include "HTNPickup.h"

void AHTNPickup::PickupDropped_Implementation()
{
	SetHidden(false);
}

void AHTNPickup::PickupGrabbed_Implementation(AHTNCharacterBase* GrabbedBy)
{
	SetHidden(true);
	LastGrabbedBy = GrabbedBy;
}

bool AHTNPickup::CanBePickedUp(AHTNCharacterBase* Picker) const
{
	return LastGrabbedBy != nullptr;
}
