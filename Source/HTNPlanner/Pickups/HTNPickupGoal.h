﻿#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "HTNPickupGoal.generated.h"

class UBoxComponent;

/** Actor that detects when a pickup enters its bounds and scores the character that drop it there */
UCLASS(Blueprintable)
class HTNPLANNER_API AHTNPickupGoal : public AActor
{
	GENERATED_BODY()

public:
	AHTNPickupGoal();

	/** Initializes the bound checks */
	virtual void PostInitializeComponents() override;

protected:
	/** Finalizes the bound checks */
	virtual void BeginDestroy() override;

	/** Waits before executing the next trace */
	void SetTimerForNextBoxTrace();

	/** Makes an async trace with the same shape as the BoxTrace */
	void BoxTrace();

	/** Checks whether there are any pick up inside the box trace. Scores the AI that dropped the pick up */
	void OnTraceFinished(const FTraceHandle& TraceHandle, FOverlapDatum& OverlapDatum);

private:
	/** Used to get the shape for the box trace */
	UPROPERTY(EditAnywhere, Category = "HTN")
	TObjectPtr<UBoxComponent> BoxComponent;

	/** The score given for positioning a pick up inside the actor bounds */
	UPROPERTY(EditAnywhere, Category = "HTN")
	float ScoreByPickup = 10.0f;

	/** The time between each async box trace */
	UPROPERTY(EditAnywhere, Category = "HTN|Trace")
	float TimeBetweenTraces = 0.5f;

	/** Used to handle the time between each trace */
	FTimerHandle TraceTimer;

	/** Used to handle the async box trace */
	FTraceHandle AsyncTraceHandle;
};
