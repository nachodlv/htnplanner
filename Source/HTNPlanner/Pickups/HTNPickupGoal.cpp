﻿
#include "HTNPickupGoal.h"

// UE Includes
#include "Components/BoxComponent.h"

// HTN Includes
#include "HTNPickup.h"
#include "HTNPlanner/Characters/HTNCharacterBase.h"

AHTNPickupGoal::AHTNPickupGoal()
{
	PrimaryActorTick.bCanEverTick = false;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	BoxComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	SetRootComponent(BoxComponent);
}

void AHTNPickupGoal::SetTimerForNextBoxTrace()
{
	const FTimerDelegate Delegate = FTimerDelegate::CreateUObject(this, &AHTNPickupGoal::BoxTrace);
	GetWorldTimerManager().SetTimer(TraceTimer, Delegate, TimeBetweenTraces, false);
}

void AHTNPickupGoal::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	SetTimerForNextBoxTrace();

}

void AHTNPickupGoal::BeginDestroy()
{
	if (GetWorld())
	{
		GetWorldTimerManager().ClearTimer(TraceTimer);
	}
	AsyncTraceHandle.Invalidate();
	Super::BeginDestroy();
}

void AHTNPickupGoal::BoxTrace()
{
	const FTransform BoxTransform = BoxComponent->GetComponentTransform();
	const FVector Location = BoxTransform.GetLocation();
	const FQuat Rotation = BoxTransform.Rotator().Quaternion();
	FCollisionObjectQueryParams ObjectParams;
	ObjectParams.AddObjectTypesToQuery(ECollisionChannel::ECC_Visibility);
	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(this);
	const FCollisionShape BoxShape = FCollisionShape::MakeBox(BoxComponent->GetScaledBoxExtent() / 2.0f);
	FOverlapDelegate Delegate = FOverlapDelegate::CreateUObject(this, &AHTNPickupGoal::OnTraceFinished);
	AsyncTraceHandle = GetWorld()->AsyncOverlapByChannel(Location, Rotation, ECollisionChannel::ECC_Visibility, BoxShape, QueryParams,
		FCollisionResponseParams::DefaultResponseParam, &Delegate);
	DrawDebugBox(GetWorld(), Location, BoxComponent->GetScaledBoxExtent(), FColor::Red, false, TimeBetweenTraces);
}

void AHTNPickupGoal::OnTraceFinished(const FTraceHandle& TraceHandle, FOverlapDatum& OverlapDatum)
{
	if (!AsyncTraceHandle.IsValid())
	{
		return;
	}

	for (FOverlapResult& OverlapResult : OverlapDatum.OutOverlaps)
	{
		AHTNPickup* Pickup = Cast<AHTNPickup>(OverlapResult.GetActor());
		if (!Pickup)
		{
			continue;
		}
		if (AHTNCharacterBase* LastGrabbedByActor = Pickup->GetLastGrabbedByActor())
		{
			LastGrabbedByActor->AddScore(ScoreByPickup);
			Pickup->Destroy();
		}
	}

	SetTimerForNextBoxTrace();
}



