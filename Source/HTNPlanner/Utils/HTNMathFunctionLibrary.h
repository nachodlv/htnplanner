﻿#pragma once

#include "CoreMinimal.h"

#include "HTNMathFunctionLibrary.generated.h"

/** Math utils for cpp and blueprints */
UCLASS()
class HTNPLANNER_API UHTNMathFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	/** Returns the angle in degrees between two non normalized vectors */
	UFUNCTION(BlueprintPure)
	static float GetAngleBetweenVectors(const FVector& Lhs, const FVector& Rhs);
};


#define HTNMath UHTNMathFunctionLibrary
