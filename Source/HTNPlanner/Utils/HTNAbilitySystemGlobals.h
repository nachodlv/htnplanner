﻿#pragma once

// UE Includes
#include "AbilitySystemGlobals.h"
#include "CoreMinimal.h"
#include "GameplayTagsManager.h"

#include "HTNAbilitySystemGlobals.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogHTN, Log, All);

#define ADD_HTN_TAG(__Tag, __TagName) AddNativeTag(TagsManager, __Tag, FName(TEXT(__TagName)));
#define HTN_TAG(__Tag) UHTNAbilitySystemGlobals::GetGlobalTags().__Tag

struct FHTNGlobalTags {

	FHTNGlobalTags();

	// Gameplay abilities
	FGameplayTag AbilityDropPickup;

	void AddNativeTag(UGameplayTagsManager& TagManager, FGameplayTag& Tag, FName TagName);
};

UCLASS()
class HTNPLANNER_API UHTNAbilitySystemGlobals : public UAbilitySystemGlobals
{
	GENERATED_BODY()

public:
	/** Returns all the gameplay ability tags used in the HTN project */
	static const FHTNGlobalTags& GetGlobalTags();
};
