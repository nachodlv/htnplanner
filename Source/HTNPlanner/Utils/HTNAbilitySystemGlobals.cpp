﻿
#include "HTNAbilitySystemGlobals.h"

DEFINE_LOG_CATEGORY(LogHTN);

FHTNGlobalTags::FHTNGlobalTags()
{
	UGameplayTagsManager& TagsManager = UGameplayTagsManager::Get();
	ADD_HTN_TAG(AbilityDropPickup, "Ability.DropPickup");
}

void FHTNGlobalTags::AddNativeTag(UGameplayTagsManager& TagManager, FGameplayTag& Tag, FName TagName)
{
	// For editor builds, hot reload triggers an ensure because it clears static variables and such.
	#if WITH_EDITOR
	Tag = TagManager.RequestGameplayTag(TagName, false);
	if (!Tag.IsValid())
	{
		Tag = TagManager.AddNativeGameplayTag(TagName);
	}
#else
	Tag = TagManager.AddNativeGameplayTag(TagName);
#endif // WITH_EDITOR
}

const FHTNGlobalTags& UHTNAbilitySystemGlobals::GetGlobalTags()
{
	static FHTNGlobalTags Tags;
	return Tags;
}
