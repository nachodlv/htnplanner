﻿#include "HTNMathFunctionLibrary.h"

float UHTNMathFunctionLibrary::GetAngleBetweenVectors(const FVector& Lhs, const FVector& Rhs)
{
	const FVector NormalizedLhs = Lhs.GetSafeNormal();
	const FVector NormalizedRhs = Rhs.GetSafeNormal();

	const float Radians = FMath::Acos(FVector::DotProduct(NormalizedLhs, NormalizedRhs));
	return FMath::RadiansToDegrees(Radians);
}
