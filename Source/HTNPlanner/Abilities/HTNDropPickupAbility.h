﻿#pragma once

// UE Includes
#include "Abilities/GameplayAbility.h"
#include "CoreMinimal.h"

#include "HTNDropPickupAbility.generated.h"

/** Drops the current pickup grabbed by the ability owner */
UCLASS()
class HTNPLANNER_API UHTNDropPickupAbility : public UGameplayAbility
{
	GENERATED_BODY()
public:
	UHTNDropPickupAbility();

	/** Check whether the ability owner has a pickup to drop */
	virtual bool CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags,
		FGameplayTagContainer* OptionalRelevantTags) const override;

	/** Drops the pickup in front of the ability owner */
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

private:
	UPROPERTY(EditAnywhere, Category = "HTN")
	float DropDistance = 200.0f;
};
