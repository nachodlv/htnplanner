﻿#include "HTNPickupAbility.h"

// UE Includes
#include "AbilitySystemComponent.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "VisualLogger/VisualLogger.h"

// HTN Includes
#include "HTNPlanner/AI/HTNAIGlobal.h"
#include "HTNPlanner/Characters/HTNCharacterBase.h"
#include "HTNPlanner/Pickups/HTNPickup.h"
#include "HTNPlanner/Utils/HTNAbilitySystemGlobals.h"
#include "HTNPlanner/Utils/HTNMathFunctionLibrary.h"

UHTNPickupAbility::UHTNPickupAbility()
{
	PickupClass = AHTNPickup::StaticClass();	
}

void UHTNPickupAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	AHTNCharacterBase* OwnerActor = CastChecked<AHTNCharacterBase>(ActorInfo->OwnerActor.Get());
	if (OwnerActor->GetPickup())
	{
		// If we currently have a pickup, try to drop it
		OwnerActor->GetAbilitySystemComponent()->TryActivateAbilitiesByTag(FGameplayTagContainer(HTN_TAG(AbilityDropPickup)));
	}

	AHTNPickup* Pickup = nullptr;
	
	if (AAIController* AIController = OwnerActor->GetController<AAIController>())
	{
		const UBlackboardComponent* BBComp = AIController->GetBlackboardComponent();
		Pickup = Cast<AHTNPickup>(BBComp->GetValueAsObject(UHTNAIGlobal::PickupToGrabBBKey));
	}

	if (!Pickup)
	{
		const TArray<FHitResult> Hits = GetNearActors(OwnerActor);
		Pickup = GetBestPickup(OwnerActor, Hits);
	}

	if (!Pickup)
	{
		// No pickup was found near the ability owner, cancel the ability
		CancelAbility(Handle, ActorInfo, ActivationInfo, true);
		return;
	}

	// Grab the pickup
	OwnerActor->GrabPickup(Pickup);

	const FVector PickupLocation = FVector(-1000.0f);
	Pickup->SetActorLocation(PickupLocation);

	UE_VLOG_UELOG(OwnerActor, LogHTN, Log, TEXT("%s picking up %s"), *OwnerActor->GetName(), *Pickup->GetName())

	EndAbility(Handle, ActorInfo, ActivationInfo, true, false);
}

TArray<FHitResult> UHTNPickupAbility::GetNearActors(const AHTNCharacterBase* OwnerActor) const
{
	const UWorld* World = OwnerActor->GetWorld();
	const FVector& OwnerLocation = OwnerActor->GetActorLocation();
	const FCollisionShape PickupSphere = FCollisionShape::MakeSphere(PickupRadius);
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(OwnerActor);
	TArray<FHitResult> Hits;
	World->SweepMultiByChannel(Hits, OwnerLocation, OwnerLocation, FQuat::Identity,
		ECollisionChannel::ECC_Visibility, PickupSphere, Params);
	return Hits;
}

AHTNPickup* UHTNPickupAbility::GetBestPickup(const AHTNCharacterBase* OwnerActor, const TArray<FHitResult>& Hits) const
{
	const FVector OwnerForward = OwnerActor->GetActorForwardVector();
	AHTNPickup* BestPickup = nullptr;
	float BestAngle = BIG_NUMBER;
	for (const FHitResult& Hit : Hits)
	{
		AHTNPickup* HitActor = Cast<AHTNPickup>(Hit.GetActor());
		if (HitActor && HitActor->IsA(PickupClass.LoadSynchronous()))
		{
			const float Angle = HTNMath::GetAngleBetweenVectors(OwnerForward, HitActor->GetActorForwardVector());
			if (Angle < BestAngle)
			{
				BestAngle = Angle;
				BestPickup = HitActor;
			}
		}
	}
	return BestPickup;
}
