﻿#include "HTNDropPickupAbility.h"

// UE Includes
#include "VisualLogger/VisualLogger.h"

// HTN Includes
#include "HTNPlanner/Characters/HTNCharacterBase.h"
#include "HTNPlanner/Pickups/HTNPickup.h"
#include "HTNPlanner/Utils/HTNAbilitySystemGlobals.h"

UHTNDropPickupAbility::UHTNDropPickupAbility()
{
	AbilityTags.AddTag(HTN_TAG(AbilityDropPickup));
}

bool UHTNDropPickupAbility::CanActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags,
	const FGameplayTagContainer* TargetTags, FGameplayTagContainer* OptionalRelevantTags) const
{
	if (Super::CanActivateAbility(Handle, ActorInfo, SourceTags, TargetTags, OptionalRelevantTags))
	{
		const AHTNCharacterBase* Character = CastChecked<AHTNCharacterBase>(ActorInfo->AvatarActor.Get());
		return Character->GetPickup() != nullptr;
	}

	return false;
}

void UHTNDropPickupAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	AHTNCharacterBase* Character = CastChecked<AHTNCharacterBase>(ActorInfo->AvatarActor.Get());
	AHTNPickup* Pickup = Character->GetPickup();
	FVector DropLocation = Character->GetActorLocation() + Character->GetActorForwardVector() * DropDistance;
	if (!Character->GetWorld()->FindTeleportSpot(Pickup, DropLocation, FRotator::ZeroRotator))
	{
		DropLocation = Character->GetActorLocation() + Character->GetActorUpVector() * DropDistance;
	}
	Pickup->SetActorLocation(DropLocation, false, nullptr, ETeleportType::TeleportPhysics);
	Character->DropPickup();

	UE_VLOG_UELOG(Character, LogHTN, Log, TEXT("%s dropping %s"), *Character->GetName(), *Pickup->GetName());

	EndAbility(Handle, ActorInfo, ActivationInfo, false, false);
}
