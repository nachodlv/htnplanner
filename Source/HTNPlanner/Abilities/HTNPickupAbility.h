﻿#pragma once

// UE Includes
#include "Abilities/GameplayAbility.h"
#include "CoreMinimal.h"

#include "HTNPickupAbility.generated.h"

class AHTNPickup;
class AHTNCharacterBase;

/** Grabs a pickup inside the PickupRadius */
UCLASS()
class HTNPLANNER_API UHTNPickupAbility : public UGameplayAbility
{
	GENERATED_BODY()

public:
	UHTNPickupAbility();

	/** Searches for pickups inside the pickup radius and grabs the one in front of the ability owner */
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

protected:
	/** Search for actors near the OwnerActor */
	TArray<FHitResult> GetNearActors(const AHTNCharacterBase* OwnerActor) const;

	/** Get the pickup that is the nearest with the front of the ability owner */
	AHTNPickup* GetBestPickup(const AHTNCharacterBase* OwnerActor, const TArray<FHitResult>& Hits) const;

private:
	/** The pickup reach */
	UPROPERTY(EditAnywhere, Category = "HTN")
	float PickupRadius = 500.0f;

	/** The pickup class that this ability will grab */
	UPROPERTY(EditAnywhere, Category = "HTN")
	TSoftClassPtr<AHTNPickup> PickupClass;
};
